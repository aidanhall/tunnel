extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
#func _ready():
#	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.


func _on_Level1_pressed():
	get_tree().change_scene("res://scenes/Level1.tscn")

func _on_Level2_pressed():
	get_tree().change_scene("res://scenes/Level2.tscn")

func _on_Level3_pressed():
	get_tree().change_scene("res://scenes/Level3.tscn")


func _on_Level4_pressed():
	get_tree().change_scene("res://scenes/Level4.tscn")


func _on_Level0_pressed():
	get_tree().change_scene("res://scenes/Level0.tscn")
