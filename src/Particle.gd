extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal won()
signal died()

export var isWave = false
var graceFrames = 0
var velocity = Vector2(0, 0)

export var bounceRatio = 0.9
export var initialVel = Vector2(0, 0)
export var slowCutoff = 0.5

var bounces = 0

func toggleWaveParticle():
	$SwitchSound.play()
	var disable
	var enable
	if isWave:
		disable = "Wave"
		enable = "Particle"
		graceFrames = 3
	else:
		enable = "Wave"
		disable = "Particle"

	get_node(disable + "Col").set_deferred("disabled", true)
	get_node(disable + "Sprite").visible = false
	get_node(enable + "Sprite").visible = true

	isWave = not isWave
	
	if not isWave:
		get_node(enable + "Col").set_deferred("disabled", false)
	else:
		$WaveTimer.wait_time = 128 / velocity.length()
		$WaveTimer.start()
		
	
#	if isWave:
#		position += velocity.normalized() * 128

# Called when the node enters the scene tree for the first time.
func _ready():
	print("I am not a bad particle!")
	#self.connect("body_entered", self, "_on_Particle_body_entered")
	get_node("WaveCol").set_deferred("disabled", true)
	get_node("WaveSprite").visible = false
	velocity = initialVel.rotated(rotation)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	#rotation = linear_velocity.angle()
	if Input.is_action_just_pressed("toggleState"):
		print("toggling")
		toggleWaveParticle()
	
	var col = move_and_collide(velocity * delta)
	if col != null:
		if (isWave && graceFrames == 0) || graceFrames > 0:
			if (col.collider.name == "Electron"):
				print("I won!")
				emit_signal("won")
			else:
				emit_signal("died")
		else:
			print("bouncy")
			velocity = bounceRatio * velocity.bounce(col.normal)
			position += col.remainder * delta
			bounces += 1
			$ParticleCam/BounceCount.text = String(bounces)
			$BounceSound.pitch_scale = velocity.length_squared()/ initialVel.length_squared()
			$BounceSound.play()
	
	if Input.is_action_just_pressed("cancelCurrent") || velocity.length() <= initialVel.length() * slowCutoff:
		emit_signal("died")
	
	if graceFrames > 0:
		graceFrames -= 1
	rotation = velocity.angle()
	$ParticleCam.set_global_rotation(0)


func _on_WaveTimer_timeout():
	if isWave:
		get_node("WaveCol").set_deferred("disabled", false)


func _on_refire():
	queue_free()

func _on_Particle_died():
	queue_free()
	print("I died. X(")
