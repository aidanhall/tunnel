extends Node2D


var particleScene
export var initialSpeed = 300.0
export var speedIncrement = 20.0
signal fire()

# Called when the node enters the scene tree for the first time.
func _ready():
	particleScene = preload("res://scenes/Particle.tscn")
	$StartSound.play()

func _process(_delta):
	if Input.is_action_just_pressed("fire"):
		emit_signal("fire")
		
		var particle = particleScene.instance()
		particle.initialVel = Vector2(initialSpeed, 0)
		particle.rotation = $Gun.rotation
		particle.position = $Gun.position
		particle.connect("won", self, "_on_won")
		particle.connect("died", self, "_on_died")
		self.connect("fire", particle, "_on_refire")
		add_child(particle)
	if Input.is_action_just_pressed("increase_speed"):
		initialSpeed += speedIncrement
	elif Input.is_action_just_pressed("decrease_speed"):
		initialSpeed -= speedIncrement
	$Speed.text = "Speed: " + String(initialSpeed)

func _on_won():
	$WinSound.play()
	$Gun/GunCam.current = true

func _on_died():
	$Gun/GunCam.current = true
	$DeathSound.play()


func _on_WinSound_finished():
	get_tree().change_scene("res://scenes/TitleScreen.tscn")


func _on_World_fire():
	$FireSound.play()
