extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var turnSpeed = 1.0


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_pressed("ui_left"):
		rotate(-turnSpeed * delta)
	elif Input.is_action_pressed("ui_right"):
		rotate(turnSpeed * delta)
